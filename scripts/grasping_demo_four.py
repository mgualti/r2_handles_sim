#!/usr/bin/env python
'''Fourth attempt at a grasping demo, for grasping small objects.

Assembled: Northeastern University, 2015
'''

# IMPORTS ------------------------------------------------------------------------------------------

import time # Python
import signal # Python
from math import pi # Python
from copy import copy # Python
from math import acos # Python
from math import sqrt # Python
from array import array # Python

import numpy # scipy
from scipy import linalg # scipy

import rospy # ROS
import geometry_msgs.msg # ROS
import tf.transformations # ROS

import handle # NEU
from r2_mover import R2Mover # NEU
from r2_mover import CloseMode # NEU
from point_cloud import PointCloud # NEU

# SCENARIOS ----------------------------------------------------------------------------------------

def RunSingleGraspScenario(armName="right"):
  '''Detects handles and grabs a single object.'''
  
  print("Running single grasp scenario with {} arm.".format(armName))
  
  if armName=="right":
    homeConfig = (0.42, -0.45, 1.60, -2.00, -1.00, 0.30, 0.20)
    dropConfig = (0.00, -0.40, 1.60, -2.00, -1.90, 0.30, 0.00)
    neckConfig = (-0.60, -0.20, -0.15)
    normalReferencePose = numpy.array(\
      [[0.78477291, 0.61415265, -0.08335465,  0.00],
      [-0.32590814, 0.29452267, -0.89835420,  0.50],
      [-0.52717678, 0.73217000,  0.43129077, -0.27],
      [ 0.00000000, 0.00000000,  0.00000000,  1.00]])
  elif armName=="left":
    homeConfig = (-0.42, -0.45, -1.60, -2.00, 1.00, 0.30, -0.20)
    dropConfig = (-0.00, -0.40, -1.60, -2.00, 1.90, 0.30, -0.00)
    neckConfig = (-0.60, +0.20, -0.15)
    normalReferencePose = numpy.array(\
      [[0.84931769, -0.52228735, -0.07665101,  0.30],
      [ 0.28388673,  0.32948938,  0.90046936, -0.50],
      [-0.44504807, -0.78654476,  0.42811161, -0.27],
      [ 0.00000000,  0.00000000,  0.00000000,  1.00]])
  
  # Initialization
  
  mover = R2Mover(speedFactor=1.5, movePrompt=False)
  time.sleep(5.0) # wait for messaging system to catch up
  
  mover.SetVelocityLimits()
  mover.AddObstaclesMoveIt(obstacleSet=1)
  time.sleep(5.0) # wait for limits to register
  
  # Move head
  
  mover.MoveNeckToConfigTarget(neckConfig)
  
  while not rospy.is_shutdown():
  
    # Move to start
    
    mover.MoveArmToConfigTarget(armName, homeConfig)
    mover.OpenHand(armName)
    
    #qHand0, tHand0 = mover.GetPose("r2/"+armName+"_middle_base", "r2/robot_base")
    #print("End effector at position {}, orientation {}.".format(tHand0, qHand0))
    #THand0 = tf.transformations.quaternion_matrix(qHand0); THand0[0:3,3] = tHand0
    
    # Get handles and select one
    
    handles = handle.RequestHandles(addBothAxes=False)
    if len(handles) == 0:
      print("No handles found!")
      continue
    
    targHandle = handle.NearestHandle(handles, armName, normalReferencePose)
    handle.PublishHandles([targHandle], "/grasping_demo/handles", 40, False)
    
    # Execute grasp
    
    offsets = [((targHandle.radius-0.01),0.03,-(targHandle.radius+0.14))]
    
    for i, offset in enumerate(offsets):
      q, t = targHandle.GetPoseQ(offset)
      didMove = mover.MoveArmToPoseTargetMoveIt(armName, q, t)
      if not didMove: break
    if not didMove: continue
    
    mover.CloseHand(armName, CloseMode.PRECISION)
    t = (t[0], t[1], t[2]-0.20)
    mover.MoveArmToPoseTargetMoveIt(armName, q, t)
    mover.MoveArmToConfigTarget(armName, homeConfig)
    
    if not mover.movePrompt:
      raw_input("Hit Enter to open the hand...")
    
    mover.OpenHand()
    
# HELPERS ------------------------------------------------------------------------------------------

def InterruptHandler(signalMsg, frame):
  '''Callback function for when Ctrl-C is issued from the terminal while this program is running.'''
  
  print("Received interrupt.")
  exit()
  
# MAIN ---------------------------------------------------------------------------------------------

if __name__=='__main__':
  '''Python entry point when running this module as an executable.'''
  
  signal.signal(signal.SIGINT, InterruptHandler)
  rospy.init_node("grasping_demo_two", anonymous=False)
  armName = rospy.get_param("~side", "right")
  
  RunSingleGraspScenario(armName)
  
  print("Completed grasping_demo_one.")
