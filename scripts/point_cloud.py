'''Provides various tools for operating on point clouds.

Assembled: Northeastern University, 2014
'''

from math import sqrt # Python
from math import isnan # Python

import numpy # scipy
from numpy import linalg # scipy
from scipy import spatial # scipy
from scipy.spatial import cKDTree # scipy

from matplotlib import pyplot # matplotlib
from mpl_toolkits.mplot3d import Axes3D # matplotlib

import tf #ROS
import rospy # ROS
import tf.transformations # ROS
from sensor_msgs import point_cloud2 # ROS
from sensor_msgs.msg import PointCloud2 # ROS

class PointCloud:
  '''Class representing a point cloud object and operations that can be performed on it.'''
  
  # - - - - - Private and Service Methods - - - - -
  
  def __init__(self, points, normals=None):
    '''Determine input data format and convert to nx3 numpy array.
    
    - Input points: Points (list/array of (x,y,z)) or string representing a ROS topic to listen to
      for a PointCloud2 message.
    - Input normals: Not implemented yet.
    '''
    
    # Class members
    
    self.points = None
    self.normals = None
    
    # Convert to numpy array
    
    if type(points) is type(numpy.array):
      pass
    elif type(points) is type([]) or type(points[0]) is type(()):
      self.points = numpy.array(points)
    elif type(points) is type(""):
      cloudListener = rospy.Subscriber(points, PointCloud2, self.receivePointCloud)
      self.cloudReceived = False
      while not self.cloudReceived:
        rospy.sleep(0.25)
        print("Waiting for cloud on topic {}.".format(points))
      cloudListener.unregister()
    else:
      raise Exception('Unrecognized cloud type in constructor for PointCloud.')
  
  def __len__(self):
    '''Returns the number of points in the point cloud.'''
    
    return self.points.shape[0]
  
  def receivePointCloud(self, inputPoints):
    '''Callback function for PointCloud2 ROS topic listener.
    
    - Input inputPoints: A PointCloud2 message with the received points in it.
    '''
    
    if self.cloudReceived: return
    self.points = numpy.array(list(point_cloud2.read_points(inputPoints)))[:,0:3]
    self.cloudReceived = True
  
  # - - - - - Public Methods - - - - -
  
  def ApplyWorkspace(self, workspace):
    '''TODO'''
    
    if workspace is None: return
    
    inPoints = []
    for i in xrange(self.points.shape[0]):
      if self.points[i,0] < workspace[0][0] or self.points[i,0] > workspace[0][1] or \
         self.points[i,1] < workspace[1][0] or self.points[i,1] > workspace[1][1] or \
         self.points[i,2] < workspace[2][0] or self.points[i,2] > workspace[2][1]: continue
      inPoints.append(self.points[i,:])
    
    self.points = numpy.array(inPoints)
  
  def DownsampleVoxelize(self, voxelSize):
    '''TODO'''
    
    # determine min and max values for each dimension
    
    l = numpy.array([min(self.points[:,0]),min(self.points[:,1]),min(self.points[:,2])])
    h = numpy.array([max(self.points[:,0]),max(self.points[:,1]),max(self.points[:,2])])
    
    # determine a voxel index for each point
    
    vIdx2Point = {}
    n = numpy.floor((h-l) / voxelSize)
    
    for i in xrange(self.points.shape[0]):
      point = self.points[i,:]
      vIdx = tuple((((point - l) * n) / (h-l)).astype(int))
      entry = (i, point)
      if vIdx in vIdx2Point:
        vIdx2Point[vIdx].append(entry)
      else:
        vIdx2Point[vIdx] = [entry]
    
    keepPoint = numpy.zeros(self.points.shape[0], 'bool')
    
    # choose the centermost point if multiple occupy a voxel
    
    for vIdx in vIdx2Point.keys():
      
      entries = vIdx2Point[vIdx]
      if len(entries) == 1:
        keepPoint[entries[0][0]] = True
        continue
      
      vIdxNp = numpy.array(vIdx)
      p = ((vIdxNp * (h-l)) / n) + l + ((h-l) / 2*n)
      
      minI = -1; minDist = float('inf')
      for i, entry in enumerate(entries):
        d = linalg.norm(p - entry[1])
        if d < minDist: minI = i; minDist = d
      
      keepPoint[entries[minI][0]] = True
      
    # downsample
    
    inPoints = []
    for i in xrange(self.points.shape[0]):
      if keepPoint[i]:
        inPoints.append(self.points[i,:])
    
    self.points = numpy.array(inPoints)
    
  def Plot(self):
    '''TODO'''
    
    fig = pyplot.figure()
    ax = fig.add_subplot(111, projection="3d", aspect="equal")
    
    # plot points
    
    x = self.points[:,0]; y = self.points[:,1]; z = self.points[:,2]
    patches = ax.scatter(x, y, z, c='k', s=5)
    extents = (min(x),max(x), min(y),max(y), min(z),max(z))
    
    # bounding box
    
    l = (extents[1]-extents[0], extents[3]-extents[2], extents[5]-extents[4])
    c = (extents[0]+l[0]/2.0, extents[2]+l[1]/2.0, extents[4]+l[2]/2.0)
    d = 1.10*max(l) / 2.0
    
    ax.plot((c[0]+d, c[0]+d, c[0]+d, c[0]+d, c[0]-d, c[0]-d, c[0]-d, c[0]-d), \
            (c[1]+d, c[1]+d, c[1]-d, c[1]-d, c[1]+d, c[1]+d, c[1]-d, c[1]-d), \
            (c[2]+d, c[2]-d, c[2]+d, c[2]-d, c[2]+d, c[2]-d, c[2]+d, c[2]-d), \
             c='k', linewidth=0)
    
    # labels
    
    ax.set_xlabel("X (m)"); ax.set_ylabel("Y (m)"); ax.set_zlabel("Z (m)")
    ax.set_title("Point cloud with {} points.".format(len(self)))
    
    patches.set_edgecolors = patches.set_facecolors = lambda *args:None # depthshade=False
    pyplot.show(block=True)
  
  def TransformQt(self, q, t):
    '''TODO'''
    
    T = tf.transformations.quaternion_matrix(q)
    T[0:3,3] = t
    self.TransformT(T)
    return T
  
  def TransformT(self, T):
    '''TODO'''
    
    h = numpy.ones((1, self.points.shape[0]))
    p = numpy.concatenate((self.points.T, h), 0)
    self.points = numpy.dot(T, p)[0:3,:].T
  
  def TransformTf(self, fromFrame, toFrame):
    '''TODO'''
    
    tfListener = tf.TransformListener()
    foundTransform = False
    nAttempts = 1000
    
    for i in xrange(nAttempts):
      try:
        (t, q) = tfListener.lookupTransform(toFrame, fromFrame, rospy.Time(0))
        foundTransform = True
        break;
      except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
        rospy.sleep(0.01)
        continue
    
    if not foundTransform:
      raise Exception("Failed to find a transform from {} to {}.".format(fromFrame, toFrame))
    
    return self.TransformQt(q, t)
    
