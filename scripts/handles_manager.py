'''Provides a class of functions for finding and selecting handles.

Assembled: Northeastern University, 2014
'''

from math import pi # Python
from math import acos # Python
from copy import copy # Python

import numpy # scipy
from scipy import linalg # scipy
from scipy.spatial.ckdtree import cKDTree # scipy

from matplotlib import pyplot # matplotlib
from mpl_toolkits.mplot3d import Axes3D # matplotlib

import rospy # ROS
import std_msgs.msg # ROS
import geometry_msgs.msg # ROS 
import tf.transformations # ROS
import visualization_msgs.msg # ROS

import handle_detector.srv # NEU
import agile_grasp.srv # NEU
from handle import Handle # NEU

class HandlesManager:
  '''A class for managing a list of handles.'''

  # - - - - - Private and Service Methods - - - - -
  
  def __init__(self, markerTopicName):
    '''Constructor for HandlesManager.
    
    - Input rvizTopicName: Name of the topic to publish a marker array of handles to. This is for
      visualizing the handles.
    '''
    
    self.markerPub = rospy.Publisher(markerTopicName, visualization_msgs.msg.MarkerArray,
      queue_size=1)
  
  def normal_for_major_axis(self, major, handlePosition, refPosition):

    # vector from handle to hand in base frame
    vector = refPosition-handlePosition
    norm_val = linalg.norm(vector)
    unit_vector = vector/norm_val
    
    # z axis
    perp = numpy.cross(major, unit_vector)
    perp /= linalg.norm(perp)
    
    # x axis
    perp2 = numpy.cross(major, perp)
    if perp2[0] < 0: perp2=-perp2
    
    return perp2, perp
  
  def roll_for_handle(self, handle, armName, z_axis):

    M = numpy.array(((handle.normal[0], handle.axis[0], z_axis[0], 0), 
                     (handle.normal[1], handle.axis[1], z_axis[1], 0), 
                     (handle.normal[2], handle.axis[2], z_axis[2], 0), 
                     (0, 0, 0, 1)), dtype=numpy.float64)
    
    x, y, z, w = tf.transformations.quaternion_from_matrix(M)
    
    pose = geometry_msgs.msg.PoseStamped()
    pose.header.stamp = rospy.Time(0)
    pose.header.frame_id = handle.frameId
    pose.pose.position = geometry_msgs.msg.Point(0, 0, 0)
    pose.pose.orientation.x = x
    pose.pose.orientation.y = y
    pose.pose.orientation.z = z
    pose.pose.orientation.w = w
    
    tfListener = tf.TransformListener()
    
    hand_frame = "r2/"+armName+"_middle_base"
    tfListener.waitForTransform(pose.header.frame_id, hand_frame, rospy.Time(0), rospy.Duration(10.0))
    pose = tfListener.transformPose(hand_frame, pose)
    
    r_1, p_1, y_1 = tf.transformations.euler_from_quaternion(\
      [pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z, pose.pose.orientation.w])
    
    return r_1
  
  # - - - - - Public Methods - - - - -
  
  def GetHandleDictionary(self, handles):
    '''Returns a mapping from handle index to a list of similar handles.
    
    - Input handles: List of Handle objects.
    - Returns handleDict: A dictionary indexed by handleIndex with list of similar handles as values.
    '''
    
    handleDict = {}
    
    for handle in handles:
      if handle.handleIndex not in handleDict:
        handleDict[handle.handleIndex] = []
      handleDict[handle.handleIndex].append(handle)
    
    return handleDict
  
  def LimitHandleLength(self, handles, maxLength):
    '''Removes similar handles from extremes until no handle's length exceeds a given length.
    
    - Input handles: List of Handle objects. Will not be modified.
    - Input maxLength: The maximum length a handle can be.
    - Returns limitedHands: References to the old list of handles which only include the ones after
      handle length limiting has taken place.
    '''
    
    limitedHandles = []
    handleGroups = self.GetHandleDictionary(handles).values()
    
    for handleGroup in handleGroups:
      
      handlePositions = []
      groupCentroid = numpy.zeros(3)
      
      for handle in handleGroup:
        handlePositions.append(handle.position)
        groupCentroid += handle.position
      
      groupCentroid /= len(handlePositions)
      
      for handle in handleGroup:
        d = linalg.norm(handle.position-groupCentroid)
        if d < (maxLength/2.0):
          limitedHandles.append(handle)
      
      if len(limitedHandles) == 0:
        # each handle must have at least 1 hand: use the centermost one
        tree = cKDTree(handlePositions)
        d, idx = tree.query(groupCentroid)
        limitedHandles.append(handleGroup[idx])
    
    return limitedHandles

  def PlotHandles(self, handles, cloud):
    '''Plots handles appropriately position in a point cloud.
    
    - Input handles: List of Handle objects.
    - Input cloud: PointCloud object to also plot.
    '''
    
    fig = pyplot.figure()
    ax = fig.add_subplot(111, projection="3d", aspect="equal")
    
    # plot cloud
    
    x = cloud.points[:,0]; y = cloud.points[:,1]; z = cloud.points[:,2]
    patches = ax.scatter(x, y, z, c='k', s=5)
    extents = (min(x),max(x), min(y),max(y), min(z),max(z))
    
    # plot handles
    
    for handle in handles:
      ptA = handle.position - 0.20*handle.normal
      ptB = handle.position
      ptC = handle.position + 0.04*handle.axis
      ptD = handle.position
      x = (ptA[0],ptB[0],ptC[0],ptD[0])
      y = (ptA[1],ptB[1],ptC[1],ptD[1])
      z = (ptA[2],ptB[2],ptC[2],ptD[2])
      ax.plot(x, y, z, linewidth=5)
    
    # bounding box
    
    l = (extents[1]-extents[0], extents[3]-extents[2], extents[5]-extents[4])
    c = (extents[0]+l[0]/2.0, extents[2]+l[1]/2.0, extents[4]+l[2]/2.0)
    d = 1.10*max(l) / 2.0
    
    ax.plot((c[0]+d, c[0]+d, c[0]+d, c[0]+d, c[0]-d, c[0]-d, c[0]-d, c[0]-d), \
            (c[1]+d, c[1]+d, c[1]-d, c[1]-d, c[1]+d, c[1]+d, c[1]-d, c[1]-d), \
            (c[2]+d, c[2]-d, c[2]+d, c[2]-d, c[2]+d, c[2]-d, c[2]+d, c[2]-d), \
             c='k', linewidth=0)
    
    # labels
    
    ax.set_xlabel("X (m)")
    ax.set_ylabel("Y (m)")
    ax.set_zlabel("Z (m)")
    ax.set_title("Handles")
    
    patches.set_edgecolors = patches.set_facecolors = lambda *args:None # depthshade=False
    pyplot.show(block=True)

  def PublishHandles(self, handles, markerLifetime=30, waitForKey=True, color=(0.0,0.7,0.5,1.0)):
    '''Publishes the handles as a marker array for visuzalization in rviz.
    
    - Input handles: A list of Handle objects.
    - Input topicName: ROS topic name to publish the handles on.
    - Input markerLifetime: Number of seconds the marker should appear in rviz before being deleted.
    - Input waitForKey: If True waits for the user to press Enter before exiting the function.
    - Input color: (r,g,b,a) values from 0-1. An alpha value of 1 is opaque, and (1,1,1,1) is white.
    '''
    
    markerArray = visualization_msgs.msg.MarkerArray()
    timeStamp = rospy.Time.now()
    
    for hIdx, handle in enumerate(handles):
      
      marker = visualization_msgs.msg.Marker()
      marker.header.stamp = timeStamp
      marker.header.frame_id = handle.frameId
      marker.lifetime = rospy.Duration(markerLifetime)
      marker.type = marker.LINE_STRIP
      marker.action = marker.ADD
      marker.id = hIdx
      
      p1 = geometry_msgs.msg.Point()
      p2 = geometry_msgs.msg.Point()
      p3 = geometry_msgs.msg.Point()
      
      pa = handle.position - 0.20*handle.normal
      pb = handle.position
      pc = handle.position + 0.04*handle.axis
      
      p1.x = pa[0]; p1.y = pa[1]; p1.z = pa[2]
      p2.x = pb[0]; p2.y = pb[1]; p2.z = pb[2]
      p3.x = pc[0]; p3.y = pc[1]; p3.z = pc[2]
      marker.points = [p1, p2, p3]
      
      marker.color.r = color[0]; marker.color.g = color[1]
      marker.color.b = color[2]; marker.color.a = color[3] 
      marker.scale.x = 0.005; marker.scale.y = 0.005; marker.scale.z = 0.005
      
      markerArray.markers.append(marker)
    
    self.markerPub.publish(markerArray)
    
    if waitForKey:
      inputString = raw_input("Published {} handles, press Enter to continue...".format(len(handles)))
  
  def NearestHandle(self, handles, armName, refPose):
    '''Finds the handle closest to the reference pose.
    Also selects the handle axis with minimal rotation against the reference pose and adjusts the
    handle normal to minimize rotation with the reference pose.
    
    - Input handles: List of Handle objects.
    - Input armName: Name of the arm being used, "right" or "left".
    - Input refPose: 4x4 pose matrix (numpy array).
    - Returns nearestHandle: A single Handle object.
    '''
    
    handles = self.ReduceToCentermostHandles(handles)
    
    # locate nearest handle
    
    handlePositions = []
    for handle in handles:
      handlePositions.append(handle.position)
    tree = cKDTree(handlePositions)
    d, idx = tree.query(refPose[0:3,3])
    nearestHandle = handles[idx]
      
    nearestHandle1 = nearestHandle
    nearestHandle2 = Handle(nearestHandle1.position, -nearestHandle1.axis, nearestHandle1.normal,
      nearestHandle1.radius, nearestHandle1.handleIndex, nearestHandle1.frameId)
    
    nearestHandle1.normal, z_axis1 = self.normal_for_major_axis(
      nearestHandle1.axis, nearestHandle1.position, refPose[0:3,3])
    nearestHandle2.normal, z_axis2 = self.normal_for_major_axis(
      nearestHandle2.axis, nearestHandle2.position, refPose[0:3,3])
    
    # transform both handles into hand frame
    
    r1 = self.roll_for_handle(nearestHandle1, armName, z_axis1)
    r2 = self.roll_for_handle(nearestHandle2, armName, z_axis2)

    if numpy.abs(r1) < numpy.abs(r2): return nearestHandle1
    return nearestHandle2
    
  def RankHandlesByPoseAndDistance(self, handles, nearbyPoint, armName):
    '''Classifies handles by pose, then sorts them according to w-space distance from a given point.
    
    - Input handles: List of Handle objects which will be unmodified.
    - Input nearbyPoint: A point from which hands whose center is closer to will be prefered.
    - Input armName: The name of the arm being used to grasp the handle. The semantics of the axes
      of the handle are different depending on the arm.
    - Returns rankedHandles: A new list of handles sorted according to the pose and distance heuristic.
    '''
    
    # bin hands by number of known good pose qualities
    
    x = numpy.array((1,0,0))
    y = numpy.array((0,1,0))
    z = numpy.array((0,0,1))
    
    score = {}
    for handle in handles:
      a = handle.axis; n = handle.normal; c = numpy.cross(n, a); s = 0
      if not handle.IsVertical() and c[2] < 0: s += 1 # penalizes palm up grasps
      #if not handle.IsVertical() and c[2] > 0: s += 1 # penalizes palm down grasps
      if acos(numpy.dot(n,x)) > 45*(pi/180): s += 1 # not forward
      if min(acos(numpy.dot(n,z)), acos(numpy.dot(n,-z))) < 45*(pi/180): s += 2 # fingertips vertical
      if armName == "right" and handle.IsVertical() and c[1] > 0: s += 2 # vertical and palm right
      if armName == "left" and handle.IsVertical() and c[1] < 0: s += 2 # vertical and palm left
      if s not in score: score[s] = []
      score[s].append(handle)
    
    # then, sort each bin by closeness to nearbyPoint
    
    nearbyPoint = numpy.array(nearbyPoint)
    comparator = lambda x : linalg.norm(x.position-nearbyPoint)
    
    for handleBin in score.values():
      handleBin.sort(key=comparator)
    
    rankedHandles = []; validScores = score.keys(); validScores.sort()
    for idx in validScores:
      rankedHandles += score[idx]
    
    print("Handles have scores {}.".format(validScores))
    
    return rankedHandles
  
  def ReduceToCentermostHandles(self, handles):
    '''TODO'''
    
    limitedHandles = []
    handleGroups = self.GetHandleDictionary(handles).values()
    
    for handleGroup in handleGroups:
      
      handlePositions = []
      groupCentroid = numpy.zeros(3)
      
      for handle in handleGroup:
        handlePositions.append(handle.position)
        groupCentroid += handle.position
      
      groupCentroid /= len(handlePositions)
      
      tree = cKDTree(handlePositions)
      d, idx = tree.query(groupCentroid)
      limitedHandles.append(handleGroup[idx])
    
    return limitedHandles

  def RequestGrasps(self):
    '''Signals the ROS grasps service, requesting handles from the agile_grasp node.
    
    - Returns hanles: List of Handle objects found or an empty list of none were found.
    '''
    
    print("Waiting for grasps service.")
    rospy.wait_for_service("grasps_server")
    print("Grasp service is up.")
    
    try:
      HandlesSignal = rospy.ServiceProxy("grasps_server", agile_grasp.srv.FindGrasps)
      handlesMsg = HandlesSignal(0).grasps_msg
    except:
      return []
    
    handles = []; frameId = handlesMsg.header.frame_id
    for grasp in handlesMsg.grasps:
      p = (grasp.center.x, grasp.center.y, grasp.center.z)
      a = (grasp.axis.x, grasp.axis.y, grasp.axis.z)
      n = (grasp.approach.x, grasp.approach.y, grasp.approach.z)
      r = grasp.width.data
      handles.append(Handle(p,a,n,r,grasp.handleIndex,frameId))
      a = (-a[0], -a[1], -a[2])
      handles.append(Handle(p,a,n,r,grasp.handleIndex,frameId))
    
    print("Found {} handles.".format(len(handles)))
    return handles
    
  def RequestHandles(self, addBothAxes=True):
    '''Signals the ROS handles service, requesting handles from the handles_server node.
    
    - Input addBothAxes: TODO
    - Returns handles: List of the Handle objects found or an empty list if none were found.
    '''
    
    print("Waiting for handles service.")
    rospy.wait_for_service("get_handles_server")
    print("Handles service is up.")
    
    try:
      HandlesSignal = rospy.ServiceProxy("get_handles_server", handle_detector.srv.GetHandles)
      handlesMsg = HandlesSignal(0).handles_msg
    except:
      return []
    
    handles = []; frameId = handlesMsg.header.frame_id
    for hIdx, handle in enumerate(handlesMsg.handles):
      for cylinder in handle.cylinders:
        p = (cylinder.pose.position.x, cylinder.pose.position.y, cylinder.pose.position.z)
        a = (cylinder.axis.x, cylinder.axis.y, cylinder.axis.z)
        n = (cylinder.normal.x, cylinder.normal.y, cylinder.normal.z)
        r = cylinder.radius
        handles.append(Handle(p,a,n,r,hIdx,frameId))
        if addBothAxes:
          a = (-cylinder.axis.x, -cylinder.axis.y, -cylinder.axis.z)
          handles.append(Handle(p,a,n,r,hIdx,frameId))
        
    return handles
