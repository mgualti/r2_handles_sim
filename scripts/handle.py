'''Provides structures for storing information about a handle and its configuration in space.

Assembled: Northeastern University, 2014
'''

from math import pi # Python
from math import acos # Python
from copy import copy # Python

import numpy # scipy
from scipy import linalg # scipy
from scipy.spatial.ckdtree import cKDTree # scipy

import tf.transformations # ROS

class Handle:
  
  # - - - - - Private and Service Methods - - - - -
  
  def __init__(self, position, axis, normal, radius, handleIndex, frameId):
    '''Create a new Handle object.
    
    - Input position: Position of the handle (x,y,z).
    - Input axis: Axis along the cylinder or the negative of the thumb pointing up direction.
    - Input handleIndex: An ID for handles belonging to a similar group.
    - Input frameId: Frame name (in ROS TF) that these coordinates are in.
    '''
    
    # handle properties
    
    self.position = numpy.array(position)
    self.axis = numpy.array(axis)
    self.axis /= linalg.norm(self.axis)
    self.normal = numpy.array(normal)
    self.normal /= linalg.norm(self.normal)
    self.radius = radius
    self.handleIndex = handleIndex
    self.frameId = frameId
    
  # - - - - - Public Methods - - - - -
  
  def GetPoseT(self, tOffset=(0,0,0), rpyOffset=(0,0,0)):
    '''Gets both possible hand poses for this handle.
    This transform alignes x with the normal, y with the axis, and z with the cross between them.
    
    - Input tOffset: Translational offset to apply to the handle in the handle frame.
    - Input rpyOffset: Roll, pitch, yaw offset to apply to the handle in the handle frame (after
      translation).
    - Returns T: Transformation matrix (numpy array) for the handle pose.
    '''
    
    T = numpy.eye(4)
    T[0:3,0] = self.normal.squeeze()
    T[0:3,1] = self.axis.squeeze()
    T[0:3,2] = numpy.cross(T[0:3,0], T[0:3,1])
    T[0:3,3] = self.position.squeeze()
    
    for i in xrange(3):
      T[0:3,3] += tOffset[i]*T[0:3,i].squeeze()
    
    if rpyOffset[0] != 0 or rpyOffset[1] != 0 or rpyOffset[2] != 0:
      R = tf.transformations.euler_matrix(rpyOffset[0], rpyOffset[1], rpyOffset[2])
      T = numpy.dot(T, R)
    
    return T
    
  def GetPoseQ(self, tOffset=(0,0,0), rpyOffset=(0,0,0)):
    '''Gets both possbile hande poses for this handle.
    
    - Input tOffset: Translational offset to apply to the handle in the handle frame.
    - Input rpyOffset: Roll, pitch, yaw offset to apply to the handle in the handle frame (after
      translation).
    - Returns q: Quaternion (x,y,z,w) for handle orientation.
    - Returns t: Position of handle.
    '''
    
    T = self.GetPoseT(tOffset,rpyOffset)
    q = tf.transformations.quaternion_from_matrix(T)
    return q, T[0:3,3]
  
  def IsVertical(self):
    '''Checks if a hand is vertical (i.e. thumb nearly up or down with the z-axis)
    
    - Returns: True if the hand is vertical and False if the hand is horizontal.
    '''
    
    return min(acos(self.axis[2]*1), acos(self.axis[2]*-1)) <= 45*(pi/180)
    
  def TransformT(self, T):
    '''TODO'''
    
    h = numpy.ones((1,1)); r = numpy.zeros((1,1))
    transform = lambda x, a : numpy.dot(T, numpy.concatenate((x.T, a)))[0:3]
    
    self.position = transform(self.position, h)
    self.axis = transform(self.axis, r)
    self.normal = transform(self.normal, r)
