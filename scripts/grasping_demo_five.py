#!/usr/bin/env python
'''This is for a flashlight handoff demo.

Assembled: Northeastern University, 2015
'''

# IMPORTS ------------------------------------------------------------------------------------------

import time # Python
import signal # Python
from math import pi # Python
from copy import copy # Python
from math import acos # Python
from math import sqrt # Python
from array import array # Python

import numpy # scipy
from scipy import linalg # scipy

import rospy # ROS
import geometry_msgs.msg # ROS
import tf.transformations # ROS

import handle # NEU
from r2_mover import R2Mover # NEU
from r2_mover import CloseMode # NEU
from point_cloud import PointCloud # NEU

# SCENARIOS ----------------------------------------------------------------------------------------

def RunHandoffScenario():
  '''Detects handles, grabs an object, detects handles, grabs object with other hand.'''
  
  homeConfigR = (0.42, -0.45, 1.60, -2.00, -1.00, 0.30, 0.20)
  dropConfigR = (0.00, -0.40, 1.60, -2.00, -1.90, 0.30, 0.00)
  neckConfigR = (-0.60, -0.20, -0.15)
  normalReferencePoseR = numpy.array(\
    [[0.78477291, 0.61415265, -0.08335465,  0.00],
    [-0.32590814, 0.29452267, -0.89835420,  0.50],
    [-0.52717678, 0.73217000,  0.43129077, -0.30],
    [ 0.00000000, 0.00000000,  0.00000000,  1.00]])
  homeConfigL = (-0.42, -0.45, -1.60, -2.00, 1.00, 0.30, -0.20)
  dropConfigL = (-1.2488598823547363, -1.5235391855239868, -0.4772993326187134, -0.7756614685058594, 2.696911334991455,  0.5661382515790563, -0.6387070130223534)
  neckConfigL = (-0.60, +0.20, -0.15)
  normalReferencePoseL = numpy.array(\
    [[0.84931769, -0.52228735, -0.07665101,  0.00],
    [ 0.28388673,  0.32948938,  0.90046936, -0.50],
    [-0.44504807, -0.78654476,  0.42811161, -0.35],
    [ 0.00000000,  0.00000000,  0.00000000,  1.00]])
  
  # Initialization
  
  mover = R2Mover(speedFactor=1.5, movePrompt=False)
  time.sleep(5.0) # wait for messaging system to catch up
  
  mover.SetVelocityLimits()
  mover.AddObstaclesMoveIt(obstacleSet=1)
  time.sleep(5.0) # wait for limits to register
  
  # Move to start
    
  mover.MoveArmToConfigTarget("right", homeConfigR)
  mover.CloseHand("right", CloseMode.OPPOSABLE_READY)
  mover.MoveNeckToConfigTarget(neckConfigL)
  mover.MoveArmToConfigTarget("left", homeConfigL)
  mover.CloseHand("left", CloseMode.OPPOSABLE_READY)
  
  # Get handles and select one
  
  handles = handle.RequestHandles(addBothAxes=False)
  if len(handles) == 0:
    print("No handles found!")
    return
  
  targHandle = handle.NearestHandle(handles, "left", normalReferencePoseL)
  handle.PublishHandles([targHandle], "/grasping_demo/handles", 40, False)
  
  # Execute grasp
  
  print("Target radius: {}.".format(targHandle.radius))
  
  tOffsets = [(-0.12,-0.03,-0.12), (0.01,-0.09,-(targHandle.radius+0.035))]
  rOffsets = [(0,0,0), (0,0,0)]
  
  targQ = []; targT = []
  for i in xrange(len(tOffsets)):
    q, t = targHandle.GetPoseQ(tOffsets[i], rOffsets[i])
    targQ.append(q); targT.append(t)
  
  didMove = mover.MoveArmToPoseTargetMoveItMulti("left", targQ, targT)
  if not didMove: return
  
  mover.CloseHand("left", CloseMode.OPPOSABLE_GRASP, 1.2)
  t = (t[0], t[1], t[2]-0.20)
  mover.MoveArmToPoseTargetMoveIt("left", q, t)
  
  # Move to start
  
  mover.MoveArmToConfigTarget("left", dropConfigL)
  mover.MoveNeckToConfigTarget(neckConfigR)
  
  # Get handles and select one
  
  handles = handle.RequestHandles(addBothAxes=False)
  if len(handles) == 0:
    print("No handles found!")
    return
  
  targHandle = handle.NearestHandle(handles, "right", normalReferencePoseR)
  #handle.PublishHandles([targHandle], "/grasping_demo/handles", 40, False)
  
  # Execute grasp
  
  print("Target radius: {}.".format(targHandle.radius))
  
  tOffsets = [(-0.12,0.00,-0.12), (0.01,0.00,-(targHandle.radius+0.035))]
  rOffsets = [(0,0,0), (0,0,0)]
  
  targQ = []; targT = []
  for i in xrange(len(tOffsets)):
    q, t = targHandle.GetPoseQ(tOffsets[i], rOffsets[i])
    targQ.append(q); targT.append(t)
  
  didMove = mover.MoveArmToPoseTargetMoveItMulti("right", targQ, targT)
  if not didMove: return
  
  mover.CloseHand("right", CloseMode.OPPOSABLE_GRASP, 1.2)
  
  # Finsih
  
  if not mover.movePrompt:
    raw_input("Hit Enter to open the hand...")
  
  mover.OpenHand("left")
  mover.MoveArmToConfigTarget("left", homeConfigL)
  mover.MoveArmToConfigTarget("right", homeConfigR)
  
  if not mover.movePrompt:
    raw_input("Hit Enter to open the hand...")
    
  mover.OpenHand("right")
    
# HELPERS ------------------------------------------------------------------------------------------

def InterruptHandler(signalMsg, frame):
  '''Callback function for when Ctrl-C is issued from the terminal while this program is running.'''
  
  print("Received interrupt.")
  exit()
  
# MAIN ---------------------------------------------------------------------------------------------

if __name__=='__main__':
  '''Python entry point when running this module as an executable.'''
  
  signal.signal(signal.SIGINT, InterruptHandler)
  rospy.init_node("grasping_demo_five", anonymous=False)
  
  RunHandoffScenario()
  
  print("Completed grasping_demo_five.")
