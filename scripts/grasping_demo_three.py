#!/usr/bin/env python
'''Third attempt at a grasping demo, specifically for grabbing the handrail setup.

Assembled: Northeastern University, 2015
'''

# IMPORTS ------------------------------------------------------------------------------------------

import time # Python
import signal # Python
from math import pi # Python
from copy import copy # Python
from math import acos # Python
from math import sqrt # Python
from array import array # Python

import numpy # scipy
from scipy import linalg # scipy

import rospy # ROS
import geometry_msgs.msg # ROS
import tf.transformations # ROS

import handle # NEU
from r2_mover import R2Mover # NEU
from r2_mover import CloseMode # NEU
from point_cloud import PointCloud # NEU

# SCENARIOS ----------------------------------------------------------------------------------------

def RunSingleGraspScenario(armName="right"):
  '''Detects handles and grabs a single object.'''
  
  print("Running single grasp scenario with {} arm.".format(armName))
  
  if armName=="right":
    homeConfig = (0.42, -0.45, 1.60, -2.00, -1.00, 0.30, 0.20)
    neckConfig = (-0.60, -0.20, -0.15)
    normalReferencePose = numpy.array(\
      [[0.78477291, 0.61415265, -0.08335465,  0.00],
      [-0.32590814, 0.29452267, -0.89835420,  0.40],
      [-0.52717678, 0.73217000,  0.43129077, -0.25],
      [ 0.00000000, 0.00000000,  0.00000000,  1.00]])
  elif armName=="left":
    homeConfig = (0.87, -1.40, -1.83, -2.44, 1.40, 0.30, -0.20)
    neckConfig = (-0.60, +0.20, -0.15)
    normalReferencePose = numpy.array(\
      [[0.84931769, -0.52228735, -0.07665101,  0.30],
      [ 0.28388673,  0.32948938,  0.90046936, -0.40],
      [-0.44504807, -0.78654476,  0.42811161, -0.25],
      [ 0.00000000,  0.00000000,  0.00000000,  1.00]])
  
  # Initialization
  
  mover = R2Mover(speedFactor=1.5, movePrompt=True)
  time.sleep(10.0) # wait for messaging system to catch up
  
  mover.SetVelocityLimits()
  time.sleep(10.0) # wait for limits to register
  
  # Move head
  
  mover.MoveNeckToConfigTarget(neckConfig)
  
  while not rospy.is_shutdown():
  
    # Move to start
    
    mover.MoveArmToConfigTarget(armName, homeConfig)
    mover.CloseHand(armName, CloseMode.OPPOSABLE_READY)
    
    #qHand0, tHand0 = mover.GetPose("r2/"+armName+"_middle_base", "r2/robot_base")
    #print("End effector at position {}, orientation {}.".format(tHand0, qHand0))
    #THand0 = tf.transformations.quaternion_matrix(qHand0); THand0[0:3,3] = tHand0
    
    # Get handles and select one
    
    handles = handle.RequestHandles(addBothAxes=False)
    if len(handles) == 0:
      print("No handles found!")
      continue
    
    targHandle = handle.NearestHandle(handles, armName, normalReferencePose)
    handle.PublishHandles([targHandle], "/grasping_demo/handles", 40, False)
    
    # Execute grasp
    
    ySign = -1 if armName=="right" else 1
    offsets = [(-0.12,ySign*0.045,-0.10), (-0.07,ySign*0.045,-0.05), (0.005,ySign*0.045,-0.06)]
    
    for i, offset in enumerate(offsets):
      q, t = targHandle.GetPoseQ(offset)
      didMove = mover.MoveArmToPoseTargetMoveIt(armName, q, t)
      if not didMove: break
      if i == 1:
        mover.CloseHand(armName, CloseMode.OPPOSABLE_GRASP, 0.5)
    if not didMove: continue
    
    mover.CloseHand(armName, CloseMode.OPPOSABLE_GRASP, 1.0)
    mover.CloseHand(armName, CloseMode.OPPOSABLE_GRASP, 0.3)
    
    offsets = offsets[0:-1]; offsets.reverse()
    for i, offset in enumerate(offsets):
      q, t = targHandle.GetPoseQ(offset)
      didMove = mover.MoveArmToPoseTargetMoveIt(armName, q, t)
      if not didMove: break
    
# HELPERS ------------------------------------------------------------------------------------------

def InterruptHandler(signalMsg, frame):
  '''Callback function for when Ctrl-C is issued from the terminal while this program is running.'''
  
  print("Received interrupt.")
  exit()
  
# MAIN ---------------------------------------------------------------------------------------------

if __name__=='__main__':
  '''Python entry point when running this module as an executable.'''
  
  signal.signal(signal.SIGINT, InterruptHandler)
  rospy.init_node("grasping_demo_two", anonymous=False)
  armName = rospy.get_param("~side", "right")
  
  RunSingleGraspScenario(armName)
  
  print("Completed grasping_demo_one.")
