'''A class for controlling and accessing information about Robonaut 2 at a high level.

Credits:
  - https://bitbucket.org/nasa_ros_pkg/nasa_r2_simulator/wiki/Controlling%20Robonaut%20in%20R2%20Gazebo

Assembled: Northeastern University, 2015
'''

import time # Python
from math import pi # Python
from copy import copy # Python
from copy import deepcopy # Python

import numpy # scipy
from scipy import linalg # scipy

import tf # ROS
import rospy # ROS
import shape_msgs.msg # ROS
import sensor_msgs.msg # ROS
import geometry_msgs.msg # ROS
import actionlib_msgs.msg # ROS
import trajectory_msgs.msg # ROS

import moveit_commander # MOVEIT
import moveit_msgs.msg # MOVEIT
import moveit_msgs.srv # MOVEIT

import nasa_r2_common_msgs.msg # NASA

class MoveStatus:
  '''Provides enumeration of what movement took place in a move function.'''
  
  MOVED = 0 # motion commands sent to controller
  PLAN_NOT_FOUND = 1 # failed to find a plan
  USER_SKIP_REQUEST = 2 # user requested skip
  USER_EXIT_REQUEST = 3 # user requested exit
  
class CloseMode:
  '''Provides enumeration of the various available close joint configuration types.'''
  
  CLOSED = 0 # HandClose from r2_teleop/r2_helper.py
  POWER_CLOSED = 1 # HandPowerClose from r2_teleop/r2_helper.py
  POWER_THUMB_CLOSED = 2 # HandPowerCloseThumb from r2_teleop/r2_helper.py
  OPPOSABLE_READY = 3 # thumb parallel with the fingers
  OPPOSABLE_GRASP = 4 # our custom grasp to go with opposable ready
  PRECISION_READY = 5 # ready to grasp delicate objects
  PRECISION_GRASP = 6 # for grasping delicate objects

class R2Mover:
  '''A high-level interface into the Robonaut 2 robot.'''
  
  # - - - - - Private Functions - - - - -
  
  def __init__(self, speedFactor=1, movePrompt=True):
    '''Create a new R2Mover object.'''
    
    self.speedFactor = speedFactor
    self.movePrompt = movePrompt
    
    # publishers
    
    self.jointLimitsPub = rospy.Publisher("/joint_ref_settings", 
      nasa_r2_common_msgs.msg.LabeledControllerJointSettings, queue_size=1)
    
    self.poseLimitsPub = rospy.Publisher("/pose_ref_settings", 
      nasa_r2_common_msgs.msg.LabeledControllerPoseSettings, queue_size=1)
    
    self.jointTrajPub = rospy.Publisher("/joint_refs", 
      nasa_r2_common_msgs.msg.LabeledJointTrajectory, queue_size=1)
      
    self.poseTrajPub = rospy.Publisher("/pose_refs", 
      nasa_r2_common_msgs.msg.LabeledPoseTrajectory, queue_size=1)
    
    self.scenePublisher = rospy.Publisher("planning_scene",
      moveit_msgs.msg.PlanningScene, queue_size=1)
    
    self.displayTrajPub = rospy.Publisher("/move_group/display_planned_path",
      moveit_msgs.msg.DisplayTrajectory, queue_size=1)
    
    # subscribers
    
    self.jointsReceived = True
    self.goalComplete = True
    self.goalStateSub = rospy.Subscriber("/goal_status", actionlib_msgs.msg.GoalStatusArray,
      self.receiveGoalStatus)
    
    # services
    
    print("Waiting for IK service.")
    rospy.wait_for_service("compute_ik")
    print("IK service is up.")
    
    # initialize MoveIt!
    
    moveit_commander.roscpp_initialize([])
    self.robot = moveit_commander.RobotCommander()
    
    self.groups = {"right":moveit_commander.MoveGroupCommander("right_arm"),
      "left":moveit_commander.MoveGroupCommander("left_arm")}
    
    for group in self.groups.values():
      group.set_goal_orientation_tolerance(0.08)
      group.set_goal_position_tolerance(0.01)
      #group.set_planner_id("LBKPIECEkConfigDefault")
    
  def receiveJointState(self, jointState):
    '''Callback function for a joint state listener.
    
    - Input jointState: sensor_msgs.msg.JointState message received on topic.
    '''
    
    if self.jointsReceived: return
    for i, name in enumerate(self.jointNames):
      self.jointValues[i] = jointState.position[jointState.name.index(name)]
    self.jointsReceived = True
  
  def receiveGoalStatus(self, goalStatus):
    '''Callback function for a goal status listener.
    
    - Input goalStatus: actionlib_msgs.msg.GoalStatusArray message received on topic.
    '''
    
    if self.goalComplete == True: return
    self.goalComplete = len(goalStatus.status_list) > 0 and goalStatus.status_list[0].status >= 2
  
  def waitForGoalMessage(self):
    '''Blocks until a success or failure message is received from the robot goal status.'''
    
    while not self.goalComplete and not rospy.is_shutdown():
      rospy.sleep(0.03)
    
  # - - - - - Public Functions - - - - -
  
  def AddObstaclesMoveIt(self, obstacleSet=0):
    '''Adds custom obstacles to MoveIt scene, to avoid collisions when planning with MoveIt.'''
    
    table = moveit_msgs.msg.CollisionObject()
    table.header.frame_id = "world"
    table.id = "table"
    
    planningScene = moveit_msgs.msg.PlanningScene()
    
    if obstacleSet == 0:
      # table in simulation
      tablePose = geometry_msgs.msg.Pose()
      tablePose.position.x = 0.80
      tablePose.position.y = 0.00
      tablePose.position.z = 0.41
      tablePose.orientation.w = 1.0
      tableBox = shape_msgs.msg.SolidPrimitive()
      tableBox.type = tableBox.BOX;  
      tableBox.dimensions = [0.75, 1.50, 0.90]
      table.primitives.append(tableBox)
      table.primitive_poses.append(tablePose)
      table.operation = table.ADD
      planningScene.world.collision_objects.append(table)
      planningScene.is_diff = True
    elif obstacleSet == 1:
      # wooden, height adjustable table in r2 lab
      tablePose = geometry_msgs.msg.Pose()
      tablePose.position.x = 0.81
      tablePose.position.y = 0.00
      tablePose.position.z = 0.38
      tablePose.orientation.w = 1.0
      tableBox = shape_msgs.msg.SolidPrimitive()
      tableBox.type = tableBox.BOX;  
      tableBox.dimensions = [0.71, 1.02, 1.04]
      table.primitives.append(tableBox)
      table.primitive_poses.append(tablePose)
      table.operation = table.ADD
      planningScene.world.collision_objects.append(table)
      planningScene.is_diff = True
    else:
      raise Exception("Unreckognized obstacle set, {}.".format(obstacleSet))
    
    self.scenePublisher.publish(planningScene)
    
  def CloseHand(self, armName="right", closeType=0, closeAmount=1.0):
    '''Sets the hand to some predefined configuration.
    
    - Input armName: "right or "left" to specify which hand to close.
    - Input closeType: See below for the different available close types.
    - Input closeAmount: A factor multiplied by the hard-coded joint angles.
    '''
    
    jointTrajMsg = nasa_r2_common_msgs.msg.LabeledJointTrajectory()
    jointTrajMsg.originator = "r2_teleop"
    jointTrajMsg.header.frame_id = armName+"_hand"
    
    jointTrajMsg.joint_names = [\
      "r2/"+armName+"_arm/hand/index/medial",      "r2/"+armName+"_arm/hand/index/proximal",
      "r2/"+armName+"_arm/hand/index/yaw",         "r2/"+armName+"_arm/hand/middle/medial",
      "r2/"+armName+"_arm/hand/middle/proximal",   "r2/"+armName+"_arm/hand/middle/yaw",
      "r2/"+armName+"_arm/hand/ringlittle/little", "r2/"+armName+"_arm/hand/ringlittle/ring",
      "r2/"+armName+"_arm/hand/thumb/distal",      "r2/"+armName+"_arm/hand/thumb/medial",
      "r2/"+armName+"_arm/hand/thumb/proximal",    "r2/"+armName+"_arm/hand/thumb/roll"]
    
    TORAD = (pi/180)
    
    if closeType==CloseMode.CLOSED:
      configTarget = [90*TORAD, 90*TORAD, 0*TORAD,  80*TORAD, 90*TORAD, 0*TORAD, 170*TORAD,
        170*TORAD, 0*TORAD, 50*TORAD, 50*TORAD, 70*TORAD]
    elif closeType==CloseMode.POWER_CLOSED:
      configTarget = [70*TORAD, 100*TORAD, 0*TORAD, 70*TORAD, 100*TORAD, 0*TORAD, 150*TORAD, 
        150*TORAD, 0*TORAD, 0*TORAD, 0*TORAD, 100*TORAD]
    elif closeType==CloseMode.POWER_THUMB_CLOSED:
      configTarget = [70*TORAD, 100*TORAD, 0*TORAD, 70*TORAD, 100*TORAD, 0*TORAD, 150*TORAD, 
        150*TORAD, 50*TORAD, 100*TORAD, 100*TORAD, 200*TORAD]
    elif closeType==CloseMode.OPPOSABLE_READY:
      configTarget = [0*TORAD, 0*TORAD, 0*TORAD, 0*TORAD, 0*TORAD, 0*TORAD, 0*TORAD, 
        0*TORAD, 20*TORAD, 20*TORAD, 20*TORAD, 120*TORAD]
    elif closeType==CloseMode.OPPOSABLE_GRASP:
      configTarget = [80*TORAD, 100*TORAD, 10*TORAD, 80*TORAD, 100*TORAD, 10*TORAD, 160*TORAD, 
        160*TORAD, 30*TORAD, 60*TORAD, 60*TORAD, 120*TORAD]
    elif closeType==CloseMode.PRECISION_READY:
      configTarget = [0*TORAD, 0*TORAD, -10*TORAD, 0*TORAD, 0*TORAD, 0*TORAD, 0*TORAD, 
        0*TORAD, 0*TORAD, 20*TORAD, 20*TORAD, 90*TORAD]
    elif closeType==CloseMode.PRECISION_GRASP:
      configTarget = [45*TORAD, 70*TORAD, -10*TORAD, 45*TORAD, 70*TORAD, 0*TORAD, 0*TORAD, 
        0*TORAD, 40*TORAD, 35*TORAD, 30*TORAD, 100*TORAD]
    else:
      raise Exception("Unrecognized closeType, {}.".format(closeType))
    
    if armName=="left":
      configTarget[-1] = -configTarget[-1]
      configTarget[2] = -configTarget[2]
      configTarget[5] = -configTarget[5]
    elif not armName=="right":
      raise Exception("Unrecognized armName, {}.".format(armName))
    
    for i in xrange(len(configTarget)):
      configTarget[i] *= closeAmount
    
    trajPoint = trajectory_msgs.msg.JointTrajectoryPoint()
    trajPoint.positions = configTarget
    trajPoint.velocities = [0]*len(configTarget)
    trajPoint.accelerations = [0]*len(configTarget)
    jointTrajMsg.points = [trajPoint]
    
    if self.movePrompt:
      raw_input("Hit Enter to close the hand...")
    
    self.goalComplete = False
    self.jointTrajPub.publish(jointTrajMsg)
    self.waitForGoalMessage()
    print("Finished closing hand.".format(armName))
  
  def GetPose(self, fromFrame, toFrame):
    '''Gets the pose of two frames in the TF tree.
    If fromFrame=s and toFrame=b, returns the transform bTs.
    
    - Input fromFrame: String of one frame in the TF tree.
    - Input toFrame: String of another frame in the TF tree.
    - Returns q: Quaternion (x,y,z,w) orientation of fromFrame with respect to toFrame.
    - Returns t: Vector (x,y,z) position of fromFrame with respect to toFrame.
    '''
    
    tfListener = tf.TransformListener()
    foundTransform = False
    nAttempts = 1000
    
    for i in xrange(nAttempts):
      try:
        (t, q) = tfListener.lookupTransform(toFrame, fromFrame, rospy.Time(0))
        foundTransform = True
        break;
      except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
        rospy.sleep(0.01)
        continue
    
    if not foundTransform:
      raise Exception("Failed to find a transform from {} to {}.".format(fromFrame, toFrame))
    
    return q, t
  
  def IsReachable(self, armName, qTargs, tTargs):
    '''Determines if a list of poses is reachable by the middle finger's base of the specified arm.
    
    - Input armName: "right" or "left".
    - Input qTargs: List of quaternions [(x,y,z,w), ...], in r2/robot_base.
    - Input tTargs: List of positoins [(x,y,z), ...], in r2/robot_base.
    - Returns isReachable: Numpy array of boolean values indicating which pose is reachable.
    '''
    
    # End effector in MoveIt! chain is palm. We use middle_base.
    q2, t2 = self.GetPose("r2/"+armName+"_palm" ,"r2/"+armName+"_middle_base")
    T2 = tf.transformations.quaternion_matrix(q2); T2[0:3,3] = t2
    
    isReachable = numpy.zeros(min(len(qTargs),len(tTargs)), 'bool')
    
    for i in xrange(len(isReachable)):
      
      T1 = tf.transformations.quaternion_matrix(qTargs[i]); T1[0:3,3] = tTargs[i]
      T = numpy.dot(T1,T2); q = tf.transformations.quaternion_from_matrix(T); t = T[0:3,3]
      
      ikRequest = moveit_msgs.msg.PositionIKRequest()
      ikRequest.group_name = armName+"_arm"
      ikRequest.pose_stamped.header.frame_id = "/r2/robot_base"
      ikRequest.pose_stamped.pose.position.x = t[0]
      ikRequest.pose_stamped.pose.position.y = t[1]
      ikRequest.pose_stamped.pose.position.z = t[2]
      ikRequest.pose_stamped.pose.orientation.x = q[0]
      ikRequest.pose_stamped.pose.orientation.y = q[1]
      ikRequest.pose_stamped.pose.orientation.z = q[2]
      ikRequest.pose_stamped.pose.orientation.w = q[3]
      ikSignal = rospy.ServiceProxy("compute_ik", moveit_msgs.srv.GetPositionIK)
      ikResult = ikSignal(ikRequest).solution
      isReachable[i] = len(ikResult.joint_state.position) > 0
    
    return isReachable
  
  def MoveArmToConfigTarget(self, armName, configTarget):
    '''Sets the joint angles of the given arm to configTarget.
    Blocks until movement is complete.
    
    - Input armName: "left" or "right".
    - Input configTarget: 7-element list with the desired joint angles.
    '''
    
    jointTrajMsg = nasa_r2_common_msgs.msg.LabeledJointTrajectory()
    jointTrajMsg.originator = "r2_teleop"
    jointTrajMsg.header.frame_id = armName+"_arm"
    for i in xrange(5):
      jointTrajMsg.joint_names.append("r2/" + armName + "_arm/joint" + str(i))
    jointTrajMsg.joint_names.append("r2/" + armName + "_arm/wrist/pitch")
    jointTrajMsg.joint_names.append("r2/" + armName + "_arm/wrist/yaw")
    trajPoint = trajectory_msgs.msg.JointTrajectoryPoint()
    trajPoint.positions = configTarget
    trajPoint.velocities = [0]*len(configTarget)
    trajPoint.accelerations = [0]*len(configTarget)
    jointTrajMsg.points = [trajPoint]
    
    if self.movePrompt:
      raw_input("Hit Enter to move the " + armName + " arm...")
    
    self.goalComplete = False
    self.jointTrajPub.publish(jointTrajMsg)
    self.waitForGoalMessage()
    print("Finished moving {} arm to configuration target.".format(armName))
  
  def MoveArmToPoseTarget(self, armName, q, t):
    '''Sends the desired arm pose to robodyn.
    
    - Input armName: Name of the arm to control, can be "right" or "left".
    - Input q: Quaternion in (x,y,z,w) format.
    - Input t: Translation in (x,y,z).
    '''
    
    poseTrajMsg = nasa_r2_common_msgs.msg.LabeledPoseTrajectory()
    poseTrajMsg.nodes.append("r2/" + armName + "_middle_base")
    poseTrajMsg.refFrames.append("r2/robot_base")
    
    priorityMsg = nasa_r2_common_msgs.msg.PriorityArray()
    priorityMsg.axis_priorities=[priorityMsg.LOW]*6
    poseTrajMsg.node_priorities.append(priorityMsg)
    
    pointTrajMsg = nasa_r2_common_msgs.msg.PoseTrajectoryPoint()
    poseMsg = geometry_msgs.msg.Pose()
    poseMsg.position.x = t[0]
    poseMsg.position.y = t[1]
    poseMsg.position.z = t[2]
    poseMsg.orientation.x = q[0]
    poseMsg.orientation.y = q[1]
    poseMsg.orientation.z = q[2]
    poseMsg.orientation.w = q[3]
    pointTrajMsg.positions.append(poseMsg)
    poseTrajMsg.points.append(pointTrajMsg)
    
    if self.movePrompt:
      raw_input("Hit Enter to move the " + armName + " arm...")
    
    self.poseTrajPub.publish(poseTrajMsg)
    self.waitForGoalMessage()
    print("Finsihed moving {} arm to pose target.".format(armName))
  
  def MoveArmToPoseTargetMoveIt(self, armName, q2, t2):
    '''Creates and shows a plan in MoveIt; allows user to accept and execute, replan, or exit.
    
    - Input armName: "right" or "left".
    - Input q2: Quaternion (x,y,z,w) orientation part of pose of end effector (middle_base).
    - Input t2: Translation part of pose of end effector.
    - Returns didExecute: False if execution was aborted.
    '''
    
    group = self.groups[armName]
    
    q1, t1 = self.GetPose("r2/robot_base", self.robot.get_planning_frame())
    q3, t3 = self.GetPose("r2/"+armName+"_palm" ,"r2/"+armName+"_middle_base")
    
    T1 = tf.transformations.quaternion_matrix(q1); T1[0:3,3] = t1
    T2 = tf.transformations.quaternion_matrix(q2); T2[0:3,3] = t2
    T3 = tf.transformations.quaternion_matrix(q3); T3[0:3,3] = t3
    
    T = numpy.dot(numpy.dot(T1,T2),T3)
    q = tf.transformations.quaternion_from_matrix(T); t = T[0:3,3]
    
    poseMsg = geometry_msgs.msg.Pose()
    poseMsg.position.x = t[0]
    poseMsg.position.y = t[1]
    poseMsg.position.z = t[2]
    poseMsg.orientation.x = q[0]
    poseMsg.orientation.y = q[1]
    poseMsg.orientation.z = q[2]
    poseMsg.orientation.w = q[3]
    group.set_pose_target(poseMsg)
    
    userInput = "r"
    while userInput == 'r' or userInput == 'R':
      plan = group.plan()
      if len(plan.joint_trajectory.points) == 0:
        print("Failed to find a plan!")
      userInput = raw_input("Hit Enter to accept plan, R to replan, or E to exit...")
    
    if len(plan.joint_trajectory.points) == 0 or userInput == 'e' or userInput == 'E':
      return False
    
    lastTime = rospy.Time(0)
    for point in plan.joint_trajectory.points:
      point.time_from_start = lastTime + rospy.Duration.from_sec(5.0)
    
    if self.movePrompt:
      raw_input("Hit Enter to move the " + armName + " arm...")
    
    group.execute(plan)
    rospy.sleep(2.0)
    print("Finished moving {} arm through MoveIt! waypoints.".format(armName))
    return True
  
  def MoveArmToPoseTargetMoveItMulti(self, armName, q, t):
    '''Same as MoveArmToPoseTargetMoveIt but with multiple waypoints included in the path.
    
    - Input armName: "right" or "left".
    - Input q: List of quaternions [(x,y,z,w)_1, ...] of end effector rotation.
    - Input t: List of translations [(x,y,z)_1, ...] of end effector positions.
    - Returns flag: 0 if moved, 1 if no plan, 2 if exit request, 3 if skip request.
    '''
    
    # Adjust poses for input to MoveIt!
    
    q1, t1 = self.GetPose("r2/robot_base", self.robot.get_planning_frame())
    q3, t3 = self.GetPose("r2/"+armName+"_palm" ,"r2/"+armName+"_middle_base")
    T1 = tf.transformations.quaternion_matrix(q1); T1[0:3,3] = t1
    T3 = tf.transformations.quaternion_matrix(q3); T3[0:3,3] = t3
    
    targPoses = []
    for i in xrange(min(len(q),len(t))):
      T2 = tf.transformations.quaternion_matrix(q[i]); T2[0:3,3] = t[i]
      T = numpy.dot(numpy.dot(T1,T2),T3)
      targPoses.append(numpy.concatenate(\
        (T[0:3,3], tf.transformations.quaternion_from_matrix(T))).tolist())
    
    # Create a plan for each pose and concatenate
    
    group = self.groups[armName]
    
    userInput = "r"
    while userInput == 'r' or userInput == 'R':
      
      group.set_start_state_to_current_state(); points = []; plans = []
      for i, pose in enumerate(targPoses):
        group.set_pose_target(pose); plan = group.plan()
        if len(plan.joint_trajectory.points) == 0:
          return MoveStatus.PLAN_NOT_FOUND
        plans.append(plan)
        points += plan.joint_trajectory.points
        if i > 0: points = points[1:-1]
        newJointState = sensor_msgs.msg.JointState()
        newJointState.name = plan.joint_trajectory.joint_names
        newJointState.position = points[-1].positions
        newRobotState = moveit_msgs.msg.RobotState()
        newRobotState.joint_state = newJointState
        group.set_start_state(newRobotState)
      
      plan = deepcopy(plan)
      plan.joint_trajectory.points = points
      displayTrajectory = moveit_msgs.msg.DisplayTrajectory()
      displayTrajectory.trajectory_start = self.robot.get_current_state()
      displayTrajectory.trajectory.append(plan)
      self.displayTrajPub.publish(displayTrajectory)
      userInput = raw_input("Hit Enter to accept plan, R to replan, S to skip, or E to exit...")
    
    group.clear_pose_targets()
    if userInput == 'e' or userInput == 'E': return MoveStatus.USER_EXIT_REQUEST
    if userInput == 's' or userInput == 'S': return MoveStatus.USER_SKIP_REQUEST
    
    for plan in plans:
      lastTime = rospy.Time(0)
      for point in plan.joint_trajectory.points:
        point.time_from_start = lastTime + rospy.Duration.from_sec(5)
        point.time_from_start.nsecs = 5
        lastTime = point.time_from_start
        point.velocities = []; point.accelerations = []
    
    # Execute
    
    if self.movePrompt:
      raw_input("Hit Enter to move the " + armName + " arm...")
    
    for plan in plans:
      group.execute(plan)
    
    print("Finished moving {} arm through MoveIt! waypoints.".format(armName))
    return MoveStatus.MOVED
  
  def MoveNeckToConfigTarget(self, configTarget):
    '''Moves the neck to the desired joint angles.
    
    - Input configTarget: 3-element list describing the desired neck joint angles in radians.
    '''
    
    jointTrajMsg = nasa_r2_common_msgs.msg.LabeledJointTrajectory()
    jointTrajMsg.originator = "r2_teleop"
    jointTrajMsg.header.frame_id = "neck"
    for i in xrange(3):
      jointTrajMsg.joint_names.append("r2/neck/joint" + str(i))
    trajPoint = trajectory_msgs.msg.JointTrajectoryPoint()
    trajPoint.positions = configTarget
    trajPoint.velocities = [0]*len(configTarget)
    trajPoint.accelerations = [0]*len(configTarget)
    jointTrajMsg.points = [trajPoint]
    
    if self.movePrompt:
      raw_input("Hit Enter to move the neck...")
    
    self.goalComplete = False
    self.jointTrajPub.publish(jointTrajMsg)
    self.waitForGoalMessage()
    print("Finished moving neck to configuration target.")
  
  def MoveWaistToConfigTarget(self, configTarget):
    '''Moves the waist to the desired coniguration.
    
    - Input configTarget: 1-element list of the desired waist position in radians.
    '''
    
    jointTrajMsg = nasa_r2_common_msgs.msg.LabeledJointTrajectory()
    jointTrajMsg.originator = "r2_teleop"
    jointTrajMsg.header.frame_id = "waist"
    jointTrajMsg.joint_names.append("r2/waist/joint0")
    trajPoint = trajectory_msgs.msg.JointTrajectoryPoint()
    trajPoint.positions = configTarget
    trajPoint.velocities = [0]*len(configTarget)
    trajPoint.accelerations = [0]*len(configTarget)
    jointTrajMsg.points = [trajPoint]
    
    if self.movePrompt:
      raw_input("Hit Enter to move the waist...")
    
    self.goalComplete = False
    self.jointTrajPub.publish(jointTrajMsg)
    self.waitForGoalMessage()
    print("Finished moving waist.")
  
  def OpenHand(self, armName="right"):
    '''Opens the hand on the specified arm.
    
    - Input armName: "right" or "left".
    '''
    
    jointTrajMsg = nasa_r2_common_msgs.msg.LabeledJointTrajectory()
    jointTrajMsg.originator = "r2_teleop"
    jointTrajMsg.header.frame_id = armName+"_hand"
    
    jointTrajMsg.joint_names = [\
      "r2/"+armName+"_arm/hand/index/medial",      "r2/"+armName+"_arm/hand/index/proximal",
      "r2/"+armName+"_arm/hand/index/yaw",         "r2/"+armName+"_arm/hand/middle/medial",
      "r2/"+armName+"_arm/hand/middle/proximal",   "r2/"+armName+"_arm/hand/middle/yaw",
      "r2/"+armName+"_arm/hand/ringlittle/little", "r2/"+armName+"_arm/hand/ringlittle/ring",
      "r2/"+armName+"_arm/hand/thumb/distal",      "r2/"+armName+"_arm/hand/thumb/medial",
      "r2/"+armName+"_arm/hand/thumb/proximal",    "r2/"+armName+"_arm/hand/thumb/roll"]
    
    trajPoint = trajectory_msgs.msg.JointTrajectoryPoint()
    trajPoint.positions = [0]*len(jointTrajMsg.joint_names)
    trajPoint.velocities = [0]*len(jointTrajMsg.joint_names)
    trajPoint.accelerations = [0]*len(jointTrajMsg.joint_names)
    jointTrajMsg.points = [trajPoint]
    
    if self.movePrompt:
      raw_input("Hit Enter to open the hand...")
    
    self.goalComplete = False
    self.jointTrajPub.publish(jointTrajMsg)
    self.waitForGoalMessage()
    print("Finished opening hand.")
  
  def SetVelocityLimits(self):
    '''Sets all of the velocity and acceleration limits required before the robot can be moved.'''
    
    jointVelMsg = nasa_r2_common_msgs.msg.LabeledControllerJointSettings()
    jointVelMsg.originator = "r2_teleop"
    
    # neck names
    for i in xrange(3):
      jointVelMsg.joint_names.append("r2/neck/joint" + str(i))
    
    # waist joint names
    jointVelMsg.joint_names.append("r2/waist/joint0")
    
    for armName in ("right", "left"):
      
      # arm joint names
      for i in xrange(5):
        jointVelMsg.joint_names.append("r2/"+armName+"_arm/joint" + str(i))
      
      # wrist joint names
      jointVelMsg.joint_names.append("r2/"+armName+"_arm/wrist/pitch")
      jointVelMsg.joint_names.append("r2/"+armName+"_arm/wrist/yaw")
      
      # hand joint names
      jointVelMsg.joint_names += [\
      "r2/"+armName+"_arm/hand/index/medial",      "r2/"+armName+"_arm/hand/index/proximal",
      "r2/"+armName+"_arm/hand/index/yaw",         "r2/"+armName+"_arm/hand/middle/medial",
      "r2/"+armName+"_arm/hand/middle/proximal",   "r2/"+armName+"_arm/hand/middle/yaw",
      "r2/"+armName+"_arm/hand/ringlittle/little", "r2/"+armName+"_arm/hand/ringlittle/ring",
      "r2/"+armName+"_arm/hand/thumb/distal",      "r2/"+armName+"_arm/hand/thumb/medial",
      "r2/"+armName+"_arm/hand/thumb/proximal",    "r2/"+armName+"_arm/hand/thumb/roll"]
    
    jointVelMsg.jointVelocityLimits = [0.5*self.speedFactor]*len(jointVelMsg.joint_names)
    jointVelMsg.jointAccelerationLimits = [0.5*self.speedFactor]*len(jointVelMsg.joint_names)
    
    # hands should be faster
    for i, name in enumerate(jointVelMsg.joint_names):
      if name.find("hand") < 0: continue
      jointVelMsg.jointVelocityLimits[i] *= 5
      jointVelMsg.jointAccelerationLimits[i] *= 5
    
    self.jointLimitsPub.publish(jointVelMsg)
    
    poseVelMsg = nasa_r2_common_msgs.msg.LabeledControllerPoseSettings()
    poseVelMsg.originator = "r2_teleop"
    poseVelMsg.maxLinearVelocity = 0.05*self.speedFactor
    poseVelMsg.maxRotationalVelocity = 0.1*self.speedFactor
    poseVelMsg.maxLinearAcceleration = 0.1*self.speedFactor
    poseVelMsg.maxRotationalAcceleration = 0.1*self.speedFactor
    self.poseLimitsPub.publish(poseVelMsg)
    
