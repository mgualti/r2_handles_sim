#!/usr/bin/env python
'''First grasping demo with robot which uses the AGILE grasp package.

Assembled: Northeastern University, 2015
'''

# IMPORTS ------------------------------------------------------------------------------------------

import time # Python
import signal # Python
from math import pi # Python
from copy import copy # Python
from math import acos # Python
from math import sqrt # Python
from array import array # Python

import numpy # scipy
from scipy import linalg # scipy

import rospy # ROS
import geometry_msgs.msg # ROS
import tf.transformations # ROS

from r2_mover import R2Mover # NEU
from r2_mover import CloseMode # NEU
from r2_mover import MoveStatus # NEU
from point_cloud import PointCloud # NEU
from handles_manager import HandlesManager # NEU

# SCENARIOS ----------------------------------------------------------------------------------------

def RunSingleGraspScenario(armName="right"):
  '''Detects handles and grabs a single object.'''
  
  print("Running single grasp scenario with {} arm.".format(armName))
  
  if armName=="right":
    homeConfig = (0.42, -0.45, 1.60, -2.00, -1.00, 0.30, 0.20)
    dropConfig = (0.00, -0.40, 1.60, -2.00, -1.90, 0.30, 0.00)
    neckConfig = (-0.60, -0.20, -0.15)
    normalReferencePose = numpy.array(\
      [[0.78477291, 0.61415265, -0.08335465,  0.00],
      [-0.32590814, 0.29452267, -0.89835420,  0.50],
      [-0.52717678, 0.73217000,  0.43129077, -0.40],
      [ 0.00000000, 0.00000000,  0.00000000,  1.00]])
  elif armName=="left":
    homeConfig = (-0.42, -0.45, -1.60, -2.00, 1.00, 0.30, -0.20)
    dropConfig = (-0.00, -0.40, -1.60, -2.00, 1.90, 0.30, -0.00)
    neckConfig = (-0.60, +0.20, -0.15)
    normalReferencePose = numpy.array(\
      [[0.84931769, -0.52228735, -0.07665101,  0.00],
      [ 0.28388673,  0.32948938,  0.90046936, -0.50],
      [-0.44504807, -0.78654476,  0.42811161, -0.40],
      [ 0.00000000,  0.00000000,  0.00000000,  1.00]])
  
  # Initialization
  
  handlesManager = HandlesManager("/grasping_demo/handles")
  mover = R2Mover(speedFactor=2.0, movePrompt=False)
  time.sleep(5.0) # wait for messaging system to catch up
  
  mover.SetVelocityLimits()
  mover.AddObstaclesMoveIt(obstacleSet=1)
  time.sleep(5.0) # wait for limits to register
  
  # Move head
  
  mover.MoveNeckToConfigTarget(neckConfig)
  
  while not rospy.is_shutdown():
  
    # Move to start
    
    mover.MoveArmToConfigTarget(armName, homeConfig)
    mover.CloseHand(armName, CloseMode.PRECISION_READY)
    
    # Get handles and select one
    
    handles = handlesManager.RequestGrasps()
    
    handles = handlesManager.LimitHandleLength(handles, 0.02)
    handles = handlesManager.RankHandlesByPoseAndDistance(
      handles, normalReferencePose[0:3,3], armName)
    handlesManager.PublishHandles(handles, 30, False, (0.1,0.6,0.5,0.5))
    
    if len(handles) == 0:
      print("No handles found!")
      continue
    
    # Execute grasp
    
    moveStatus = MoveStatus.USER_EXIT_REQUEST
    
    for targHandle in handles:
      
      # vertical affordances are very likely to have collision problems with this kind of grasp
      if targHandle.IsVertical(): continue
      # palm up is also likely to be bad for now
      if targHandle.axis[1] < 0 : continue
      
      armSign = 1 if armName=="right" else -1
      
      tOffsets = [(-0.12,armSign*0.025,-0.08), (-0.02,armSign*0.025,-0.08)]
      rOffsets = [(0,0,0), (0,0,0)]
      
      targQ = []; targT = []
      for i in xrange(len(tOffsets)):
        q, t = targHandle.GetPoseQ(tOffsets[i], rOffsets[i])
        targQ.append(q); targT.append(t)
      
      reachable = mover.IsReachable(armName, targQ, targT)
      if not reachable.all(): continue
      
      handlesManager.PublishHandles([targHandle], 20, False, (1.0,0.0,0.0,1.0))
      
      moveStatus = mover.MoveArmToPoseTargetMoveItMulti(armName, targQ, targT)
      if moveStatus == MoveStatus.MOVED or moveStatus == MoveStatus.USER_EXIT_REQUEST: break
    
    if moveStatus != MoveStatus.MOVED: continue
    
    mover.CloseHand(armName, CloseMode.PRECISION_GRASP, 1.3)
    t = (t[0], t[1], t[2]-0.20)
    mover.MoveArmToPoseTargetMoveIt(armName, q, t)
    mover.MoveArmToConfigTarget(armName, homeConfig)
    
    if not mover.movePrompt:
      raw_input("Hit Enter to open the hand...")
    
    mover.OpenHand(armName)
    mover.CloseHand(armName, CloseMode.OPPOSABLE_READY)
    
# HELPERS ------------------------------------------------------------------------------------------

def InterruptHandler(signalMsg, frame):
  '''Callback function for when Ctrl-C is issued from the terminal while this program is running.'''
  
  print("Received interrupt.")
  exit()
  
# MAIN ---------------------------------------------------------------------------------------------

if __name__=='__main__':
  '''Python entry point when running this module as an executable.'''
  
  signal.signal(signal.SIGINT, InterruptHandler)
  rospy.init_node("grasping_demo_two", anonymous=False)
  armName = rospy.get_param("~side", "right")
  
  RunSingleGraspScenario(armName)
  
  print("Completed grasping_demo_one.")
