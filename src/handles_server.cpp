#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>

// project-specific
#include <handle_detector/affordances.h>
#include <handle_detector/messages.h>
#include <handle_detector/visualizer.h>

// services
#include <handle_detector/GetHandles.h>

// messages
#include <handle_detector/CylinderArrayMsg.h>
#include <handle_detector/HandleListMsg.h>


typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;


PointCloud::Ptr g_cloud(new PointCloud);
Affordances g_affordances;
Messages g_messages;
Visualizer g_visualizer(0);
ros::Publisher g_handles_rviz_pub, g_pcl_pub;
std::string g_base_frame;
std::string g_sensor_frame;
std::string g_cloud_topic;
bool g_hasCloud = false;
bool g_applyStaticSensorToHeadOffset;
ros::NodeHandle* g_node;

void cloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg)
{
  // check whether point cloud is empty
  if (msg->height == 0 || msg->width == 0)
    return;
  
  // update the sensor frame name
  g_sensor_frame = msg->header.frame_id;
  
  // convert sensor_msg to PCL point cloud
  pcl::fromROSMsg(*msg, *g_cloud);
  
  // done
  ROS_INFO("Received cloud with %d points.", (int)g_cloud->size());
  g_hasCloud = true;
}

bool handlesCallback(handle_detector::GetHandles::Request& req, handle_detector::GetHandles::Response& resp)
{
  // get cloud
  g_hasCloud = false;
  ros::Subscriber cloudSub = g_node->subscribe(g_cloud_topic, 10, cloudCallback);
  ros::Rate waitCloudRate(10);
  while (!g_hasCloud && ros::ok())
    ros::spinOnce();
    waitCloudRate.sleep();
  cloudSub.shutdown();
  if (!ros::ok()) return false;
  
  // create identity transform
  tf::StampedTransform identity_transform(tf::Transform(tf::Quaternion(0,0,0,1)), ros::Time::now(), "", "");
 
  // lookup transform from sensor frame to base frame
  tf::StampedTransform transform, transform_base_to_head;
  tf::TransformListener transform_listener;    
  
  if (g_applyStaticSensorToHeadOffset)
  {
    ROS_INFO("getting transform with static offset %s -> %s", g_base_frame.c_str(), g_sensor_frame.c_str());
    
    // rosrun extrinsic_calibrator interactive_calibrator _parent_frame:="/r2/head" _child_frame:="/r2/asus_frame"
    // (0.12, 0.0820001, -0.00753189) (0.707095, -0.00381837, 0.707098, 0.00381575)
    
    // 2015-08-24
    // (0.0851887, 0.0974786, 0.00339578) (0.706578, -0.0321487, 0.705744, -0.0404876)
    
    // 2015-08-25
    // (0.0897354, 0.122035, -0.00753185) (0.707105, -0.0010822, 0.707107, 0.0010796)
    
    transform_listener.waitForTransform(g_base_frame, "/r2/head", ros::Time(0), ros::Duration(5.0));
    transform_listener.lookupTransform(g_base_frame, "/r2/head", ros::Time(0), transform_base_to_head);
    
    tf::StampedTransform transform_head_to_asus;
    transform_head_to_asus.setOrigin(tf::Vector3(0.0897354, 0.122035, -0.00753185));
    transform_head_to_asus.setRotation(tf::Quaternion(0.707105, -0.0010822, 0.707107, 0.0010796));
    
    tf::StampedTransform transform_base_to_asus;
    transform_base_to_asus.mult(transform_base_to_head, transform_head_to_asus);
    transform = transform_base_to_asus;
  }
  else
  {
    ROS_INFO("looking up transform %s -> %s", g_base_frame.c_str(), g_sensor_frame.c_str());
    transform_listener.waitForTransform(g_base_frame, g_sensor_frame, ros::Time(0), ros::Duration(5.0));
    transform_listener.lookupTransform(g_base_frame, g_sensor_frame, ros::Time(0), transform);
  }
  
  std::cout << "Cloud Transform (xyz): " << transform.getOrigin().x() << " " << transform.getOrigin().y() << " " << transform.getOrigin().z() << "\n";
  std::cout << "Cloud Transform (xyzw): " << transform.getRotation().getX() << " " << transform.getRotation().getY() << " " << transform.getRotation().getZ() << " " << transform.getRotation().getW() << "\n";
  
  // transform point cloud to base frame
  pcl_ros::transformPointCloud(*g_cloud, *g_cloud, transform);
    
  // search grasp affordances
  std::vector<CylindricalShell> cylindrical_shells = g_affordances.searchAffordances(g_cloud, &identity_transform);
  if (cylindrical_shells.size() == 0)
  {
    printf("No grasps found!\n");
    return false;
  }
  
  // search handles    
  std::vector< std::vector<CylindricalShell> > handles = g_affordances.searchHandles(g_cloud, cylindrical_shells);
  if (handles.size() == 0)
  {
    printf("No handles found!\n");
    return false;
  }
  
  // create response
  resp.handles_msg = g_messages.createHandleList(handles, g_base_frame);
  
  // visualize handles in Rviz
  std::vector<visualization_msgs::MarkerArray> handles_marker_array_list;
  visualization_msgs::MarkerArray handles_marker_array;
  g_visualizer.createHandles(handles, g_base_frame, handles_marker_array_list, handles_marker_array);
  
  // visualize point cloud
  PointCloud::Ptr cloud_vis(new PointCloud);
  cloud_vis = g_affordances.workspaceFilter(g_cloud, &identity_transform);
  sensor_msgs::PointCloud2 pc2msg;
  pcl::toROSMsg(*cloud_vis, pc2msg);
  pc2msg.header.stamp = ros::Time::now();
  pc2msg.header.frame_id = g_base_frame;
  g_pcl_pub.publish(pc2msg);
  
  // visualize hanles
  g_handles_rviz_pub.publish(handles_marker_array);
  
  return true;
}

int main(int argc, char **argv)
{
  // create node
  ros::init(argc, argv, "get_handles_server");
  g_node = new ros::NodeHandle("~");
  
  // create publishers and services
  g_handles_rviz_pub = g_node->advertise<visualization_msgs::MarkerArray>("handles_viz", 10);
  g_pcl_pub = g_node->advertise<sensor_msgs::PointCloud2>("cloud", 10);
  ros::ServiceServer handlesService = g_node->advertiseService("/get_handles_server", handlesCallback);
  
  // read parameters
  g_affordances.initParams(*g_node);
  
  g_node->getParam("apply_static_sensor_to_head_offset", g_applyStaticSensorToHeadOffset);
  
  double lifetime;
  g_node->getParam("update_interval", lifetime);
  g_visualizer.setMarkerLifetime(lifetime);
  
  g_node->getParam("base_frame", g_base_frame);
  g_node->getParam("cloud_topic", g_cloud_topic);
  
  // start execution
  ROS_INFO("Ready to get handles.");
  ros::spin();

  return 0;
}
